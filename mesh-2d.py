import sys

import gmsh
import numpy as np

L = int(sys.argv[1])
N = 2 ** (L + 2)
gmsh.initialize()

# parameters
R = 0.5  # radius

# mesh size
lc = np.sqrt(2.0 - 2.0 * np.cos(2.0 * np.pi / N))

gmsh.model.add(f"t{L}-2d")

# points around square
gmsh.model.geo.addPoint(1, 1, 0.0, lc, 1)
gmsh.model.geo.addPoint(-1, 1, 0.0, lc, 2)
gmsh.model.geo.addPoint(-1, -1, 0.0, lc, 3)
gmsh.model.geo.addPoint(1, -1, 0.0, lc, 4)

# lines around square
gmsh.model.geo.addLine(1, 2, 1)
gmsh.model.geo.addLine(2, 3, 2)
gmsh.model.geo.addLine(3, 4, 3)
gmsh.model.geo.addLine(4, 1, 4)

# boundary square
boundary = gmsh.model.geo.addCurveLoop([1, 2, 3, 4])

# points around circle
for j, t in enumerate(np.linspace(0, 2 * np.pi, N + 1)):
    gmsh.model.geo.addPoint(R * np.cos(t), R * np.sin(t), 0.0, lc, j + 5)

# lines around circle
for j in range(N - 1):
    gmsh.model.geo.addLine(j + 5, j + 6, j + 5)
gmsh.model.geo.addLine(N + 4, 5, N + 4)

# interface circle
interface = gmsh.model.geo.addCurveLoop([j + 5 for j in range(N)])

# 2d domains
interior = gmsh.model.geo.addPlaneSurface([interface])
exterior = gmsh.model.geo.addPlaneSurface([boundary, interface])

gmsh.model.geo.synchronize()

boundary_group = gmsh.model.addPhysicalGroup(1, [1, 2, 3, 4])
interface_group = gmsh.model.addPhysicalGroup(1, [j + 5 for j in range(N)])
interior_group = gmsh.model.addPhysicalGroup(2, [interface])
exterior_group = gmsh.model.addPhysicalGroup(2, [boundary, interface])

gmsh.model.setPhysicalName(1, boundary_group, "boundary")
gmsh.model.setPhysicalName(1, interface_group, "interface")
gmsh.model.setPhysicalName(2, interior_group, "exterior")
gmsh.model.setPhysicalName(2, exterior_group, "interior")

gmsh.model.mesh.generate(2)
gmsh.write(f"mesh/t{L}-2d.msh")

# gmsh.fltk.run()

# This should be called when you are done using the Gmsh Python API:
gmsh.finalize()
