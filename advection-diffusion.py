import sys

import numpy as np
import ufl
from firedrake import (
    COMM_WORLD,
    Constant,
    DirichletBC,
    File,
    Function,
    FunctionSpace,
    Mesh,
    MeshHierarchy,
    SpatialCoordinate,
    TestFunction,
    TrialFunction,
    as_vector,
    assemble,
    conditional,
    cos,
    div,
    dot,
    dS,
    dx,
    ge,
    grad,
    inner,
    sin,
    solve,
    sqrt,
)

from utils.bdf import bdf_coeffs
from utils.logging import pprint, trace
from utils.parameters import Parameters
from utils.utils import (
    get_mesh_tags,
    get_vertex_markers,
    iso_mesh,
    mesh_h,
    update_coords,
)

# from utils.logging import set_log_level

# set_log_level(3)

trace(f"running with COMM_WORLD.size = {COMM_WORLD.size}")

# parameters
trace("reading parameters...")
parameters = Parameters(sys.argv[1:])
meshfile: str = parameters["meshfile"]
dt: float = parameters["dt"]
order: int = parameters["order"]
bdf_order: int = parameters["bdf_order"]
end_time: float = parameters["end_time"]
repeats: int = parameters["repeats"]
timestep_factor: float = parameters["timestep_factor"]
output: bool = parameters["output"]
output_timestep: float = parameters["output_timestep"]
errorfile: str = parameters["errorfile"]
trace("reading parameters...DONE")

trace("defining exact solution and data...")


# construct exact solution
def alpha(t: float) -> float:
    return 1.0 + 0.25 * sin(np.pi * t)


def alpha_p(t: float) -> float:
    return np.pi * 0.25 * cos(np.pi * t)


def beta(t: float) -> float:
    return 1.0 + 0.25 * cos(np.pi * t)


def beta_p(t: float) -> float:
    return -np.pi * 0.25 * sin(np.pi * t)


def uI_func(x: SpatialCoordinate, t: float):
    u1 = Phi_func(x, t)
    u3 = np.prod([sin(2.0 * np.pi * xx) for xx in x])
    return sin(t) * u1 * u3


def uE_func(x: SpatialCoordinate, t: float):
    return -uI_func(x, t)


def uIt_func(x: SpatialCoordinate, t: float):
    u1 = Phi_func(x, t)
    u1t = Phit_func(x, t)
    u3 = np.prod([sin(2.0 * np.pi * xx) for xx in x])
    return cos(t) * u1 * u3 + sin(t) * u1t * u3


def uEt_func(x: SpatialCoordinate, t: float):
    return -uIt_func(x, t)


def u_func(x: SpatialCoordinate, t: float):
    return conditional(ge(Phi_func(x, t), 0), uE_func(x, t), uI_func(x, t))


# problem data
def A_I(x: SpatialCoordinate) -> float:
    return 10.0


def A_E(x: SpatialCoordinate) -> float:
    return 1.0


def B_I(x: SpatialCoordinate):
    return 5 * grad(x[0])


def B_E(x: SpatialCoordinate):
    return -5 * grad(x[0])


def C_I(x: SpatialCoordinate) -> float:
    return 1.0


def C_E(x: SpatialCoordinate) -> float:
    return 10.0


def fI_func(x: SpatialCoordinate, t: float):
    u = uI_func(x, t)
    ut = uIt_func(x, t)
    return ut - A_I(x) * div(grad(u)) + dot(B_I(x), grad(u)) + C_I(x) * u


def fE_func(x: SpatialCoordinate, t: float):
    u = uE_func(x, t)
    ut = uEt_func(x, t)
    return ut - A_E(x) * div(grad(u)) + dot(B_E(x), grad(u)) + C_E(x) * u


def g_func(x: SpatialCoordinate, t: float):
    u_I = uI_func(x, t)
    du_I = grad(u_I)

    u_E = uE_func(x, t)
    du_E = grad(u_E)

    phi = Phi_func(x, t)
    nu_tilde = grad(phi)
    nu = nu_tilde / sqrt(dot(nu_tilde, nu_tilde))

    return A_I(x) * np.dot(du_I, nu) - A_E(x) * np.dot(du_E, nu)


def X_func(x: np.ndarray, t: float) -> np.ndarray:
    def p(y):
        p0 = np.prod([1 - yy * yy for yy in y])
        p1 = sum([yy * yy for yy in y]) ** (1.0 / 3.0)
        return p0 * p1

    r = max(sqrt(sum([xx * xx for xx in x])) / 0.5, 1.0e-9)
    x2 = x / r
    d = as_vector([w * xx for w, xx in zip([alpha(t) - 1.0, beta(t) - 1.0, 0.0], x)])

    try:
        return x + d * p(x) / p(x2)
    except:
        trace("unable to continue")
        sys.exit(0)


def w_func(x: SpatialCoordinate, t: float):
    def p(y):
        p0 = np.prod([1 - yy * yy for yy in y])
        p1 = sum([yy * yy for yy in y]) ** (1.0 / 3.0)
        return p0 * p1

    r = sqrt(sum([xx * xx for xx in x])) / 0.5
    x2 = x / r
    dt = as_vector([w * xx for w, xx in zip([alpha_p(t), beta_p(t), 0.0], x)])
    return dt * p(x) / p(x2)


trace("defining exact solution and data...DONE")

trace("creating base_mesh and reading tags...")

# create base mesh
base_mesh = Mesh(meshfile)

# get tags from mesh file
tags = get_mesh_tags(meshfile)


# interface information
def Phi_func(x: SpatialCoordinate, t: float) -> float:
    """
    Level set function for interface.
    """
    dim = x.ufl_shape[0]
    weights = [alpha(t), beta(t), 1][:dim]
    return sum([xj * xj / (wj * wj) for xj, wj in zip(x, weights)]) - 0.25


def Phit_func(x: SpatialCoordinate, t: float) -> float:
    """
    Time derivative of level set function Phi.
    """
    dim = x.ufl_shape[0]
    weights = [alpha(t), beta(t), 1][0:dim]
    weightst = [alpha_p(t), beta_p(t), 0][0:dim]
    return sum(
        [
            -2.0 * xj * xj * wtj / (wj * wj * wj)
            for xj, wj, wtj in zip(x, weights, weightst)
        ]
    )


def interface_proj(x: np.ndarray) -> np.ndarray:
    """
    Projection operator for initial interface.
    """
    return 0.5 * x / np.linalg.norm(x)


trace("creating base_mesh and reading tags...DONE")

# do boundary projection
trace("doing interface projection...")
update_coords(base_mesh, tags, interface_proj)
trace("doing interface projection...DONE")

error_file = open(errorfile, "w")


for i in range(repeats):
    trace(f"starting level {i}")

    # generate isoparameter mesh
    trace("generating isoparametric mesh...")
    mesh = iso_mesh(base_mesh, tags, order, interface_proj)
    Vc = mesh.coordinates.function_space()
    coords_ptr = mesh.coordinates.dat.data
    coords0_ptr = coords_ptr.copy()
    trace("generating isoparametric mesh...DONE")

    x = SpatialCoordinate(mesh)
    dtinv = Constant(1.0 / dt)

    trace("setting up discrete problem...")
    # function spaces and test functions
    V = FunctionSpace(mesh, "Lagrange", order)
    Phi = Function(V, name="Phi")

    u = TrialFunction(V)
    v = TestFunction(V)

    fI = Function(V, name="fI")
    fE = Function(V, name="fE")
    g = Function(V, name="g")

    # ale advection
    w = Function(Vc, name="w")
    coords_old = [Function(Vc)]

    # variational form
    bdf_coeff0 = Constant(1)
    a = (
        bdf_coeff0 * dtinv * u * v * dx()
        + u * dot(w, grad(v)) * dx()
        + (A_I(x) * dot(grad(u), grad(v)) + dot(B_I(x), grad(u)) * v + C_I(x) * u * v)
        * dx(tags["interior"])
        + (A_E(x) * dot(grad(u), grad(v)) + dot(B_E(x), grad(u)) * v + C_E(x) * u * v)
        * dx(tags["exterior"])
    )
    # forcing terms
    L0 = (
        fI * v * dx(tags["interior"])
        + fE * v * dx(tags["exterior"])
        + g * v("-") * dS(tags["interface"])
    )

    # previous mass terms
    def L(eta):
        return dtinv * eta * v * dx()

    # boundray conditions
    DirBC = DirichletBC(V, 0, [tags["boundary"]])

    # set initial conditions
    t = 0.0

    u = Function(V, name="solution")
    trace("setting up discrete problem...DONE")

    trace("solving intial mesh problem...")
    for j in range(len(coords0_ptr)):
        coords_ptr[j] = X_func(coords0_ptr[j], t)
    interface_test = assemble(Phi * dS(tags["interface"]))
    assert abs(interface_test) < 1.0e-10
    trace("solving intial mesh problem...DONE")

    trace("setting initial conditions...")
    u.interpolate(u_func(x, 0))
    coords_old[0].assign(mesh.coordinates)

    # build up bdf assembled forms
    bdf_assembled_forms = [assemble(L(u))]
    trace("setting initial conditions...DONE")

    V_exact = FunctionSpace(mesh, "Lagrange", order + 2)
    u_exact = Function(V_exact, name="exact")
    file = File(f"output/adv-diff_order{order}_level{i}.pvd")
    next_output_time = 0.0

    if output:
        trace("outputting...")
        u_exact.interpolate(u_func(x, t))
        Phi.interpolate(Phi_func(x, t))
        file.write(
            u, u_exact, fI, fE, g, Phi, w, get_vertex_markers(mesh, tags, order), time=t
        )
        pprint("output written at time ", t)
        next_output_time += output_timestep
        trace("outputting...DONE")
    assert abs(interface_test) < 1.0e-10

    while t < end_time:
        t = t + dt
        trace(f"timestep: {t}")

        b_other = assemble(L(u))

        # move mesh
        trace("moving mesh...")
        for j in range(len(coords0_ptr)):
            coords_ptr[j] = X_func(coords0_ptr[j], t)
        interface_test = assemble(Phi * dS(tags["interface"]))
        assert abs(interface_test) < 1.0e-10
        trace("moving mesh...DONE")

        trace("getting pde data...")
        # get data
        fE.interpolate(fE_func(x, t))
        fI.interpolate(fI_func(x, t))
        g.interpolate(g_func(x, t))

        # update ale term
        w_degree = len(coords_old)
        w_coeffs = bdf_coeffs[w_degree]
        w.assign(
            dtinv
            * sum(
                [
                    Constant(c) * co
                    for c, co in zip(w_coeffs, [mesh.coordinates] + list(coords_old))
                ]
            )
        )
        trace("getting pde data...DONE")

        trace("assembling system...")
        # assemble forms on new domain
        b = assemble(L0, bcs=DirBC)

        # add in forms from old domains
        current_bdf_degree = len(bdf_assembled_forms)
        coeffs = bdf_coeffs[current_bdf_degree][1:]
        bdf_coeff0.assign(bdf_coeffs[current_bdf_degree][0])
        assert len(coeffs) == len(bdf_assembled_forms)
        b += sum([-Constant(c) * f for c, f in zip(coeffs, bdf_assembled_forms)])

        # assemble system matrix on new domain
        A = assemble(a, bcs=DirBC)
        trace("assembling system...DONE")

        trace("solving...")
        # solve
        solve(
            A, u.vector(), b, solver_parameters={"ksp_type": "preonly", "pc_type": "lu"}
        )
        trace("solving...DONE")

        if output and t >= next_output_time:
            trace("outputting...")
            u_exact.interpolate(u_func(x, t))
            Phi.interpolate(Phi_func(x, t))
            file.write(
                u,
                u_exact,
                fI,
                fE,
                g,
                Phi,
                w,
                get_vertex_markers(mesh, tags, order),
                time=t,
            )
            pprint("output written at time ", t)
            next_output_time += output_timestep
            trace("outputting...DONE")

        trace("storing old information (old forms, positions and solution)...")
        # update old mass forms
        if len(bdf_assembled_forms) < bdf_order:
            bdf_assembled_forms.insert(0, Function(V))
            bdf_assembled_forms[0].assign(assemble(L(u)))
        else:
            for k in range(bdf_order - 1, 0, -1):
                bdf_assembled_forms[k].assign(bdf_assembled_forms[k - 1])
            bdf_assembled_forms[0].assign(assemble(L(u)))

        # update ale velocity
        if len(coords_old) < bdf_order:
            coords_old.insert(0, Function(Vc))
            coords_old[0].assign(mesh.coordinates)
        else:
            for k in range(bdf_order - 1, 0, -1):
                coords_old[k].assign(coords_old[k - 1])
            coords_old[0].assign(mesh.coordinates)
        trace("storing old information (old forms, positions and solution)...DONE")

    # compute error
    trace("computing mesh size and dofs...")
    h = mesh_h(mesh)
    n_dofs = V.dim()
    trace("computing mesh size and dofs...DONE")

    trace("computing error...")
    u_exact.interpolate(u_func(x, t))
    l2error2 = assemble((u - u_exact) ** 2 * dx())
    l2error = sqrt(l2error2)
    h1error2 = assemble(dot(grad(u - u_exact), grad(u - u_exact)) * dx())
    h1error = sqrt(h1error2)
    trace("computing error...DONE")

    trace("outputting errors...")
    pprint(f"{h=} {dt=} {n_dofs=} {l2error=} {h1error=}")
    error_file.write(f"level{i}:\n")
    error_file.write(f"  h: {h}\n")
    error_file.write(f"  dt: {dt}\n")
    error_file.write(f"  n_dofs: {n_dofs}\n")
    error_file.write(f"  l2_error: {l2error}\n")
    error_file.write(f"  h1_error: {h1error}\n")
    trace("outputting errors...DONE\n")

    trace("refining mesh...")
    if COMM_WORLD.size == 1:
        base_mesh = MeshHierarchy(base_mesh, 1)[-1]
        update_coords(base_mesh, tags, interface_proj)
        dt *= timestep_factor
    elif repeats > 1:
        pprint("unable to refine in parallel")
    trace("refining mesh...DONE")
