import getopt
import sys
from typing import Any, Dict, List

from utils.logging import info


class Parameters:
    def __init__(self, args: List[str], verbose: bool = True):
        self._params: Dict[str, Any] = {}
        self.verbose = verbose

        # get parameters
        try:
            opts, _ = getopt.getopt(
                args,
                "",
                [
                    "mesh=",
                    "dt=",
                    "order=",
                    "bdf_order=",
                    "end_time=",
                    "repeats=",
                    "timestep_factor=",
                    "errorfile=",
                    "output",
                    "output_timestep=",
                    "errorfile=",
                ],
            )
        except getopt.GetoptError as err:
            print(err, file=sys.stderr)
            sys.exit(2)

        # default parameters
        self._params["meshfile"] = ""
        self._params["dt"] = 0.5
        self._params["order"] = 1
        self._params["bdf_order"] = 2
        self._params["end_time"] = 2.0
        self._params["repeats"] = 5
        self._params["timestep_factor"] = 0.5
        self._params["output"] = False
        self._params["output_timestep"] = 0.1
        self._params["errorfile"] = "/dev/null"

        # assign parameter values
        for o, a in opts:
            if o == "--mesh":
                self._params["meshfile"] = a
            elif o == "--dt":
                self._params["dt"] = float(a)
            elif o == "--order":
                self._params["order"] = int(a)
                assert self._params["order"] >= 1
            elif o == "--bdf_order":
                self._params["bdf_order"] = int(a)
                assert self._params["bdf_order"] >= 1
            elif o == "--end_time":
                self._params["end_time"] = float(a)
            elif o == "--repeats":
                self._params["repeats"] = int(a)
            elif o == "--output":
                self._params["output"] = True
            elif o == "--output_timestep":
                self._params["output_timestep"] = float(a)
            elif o == "--errorfile":
                self._params["errorfile"] = a
            else:
                assert False, f"unhandled option: {o}"

    def __getitem__(self, key: str) -> Any:
        value = self._params[key]
        info(f"{key}: {value}")
        return value
