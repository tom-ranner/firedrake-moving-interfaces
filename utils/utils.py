from typing import Callable, Dict

import numpy as np
from firedrake import (
    MAX,
    READ,
    RW,
    WRITE,
    Function,
    FunctionSpace,
    Mesh,
    VectorFunctionSpace,
    ds,
    dx,
    par_loop,
)

from utils.logging import trace


def get_mesh_tags(filename: str) -> Dict[str, int]:
    """
    Recover tags for msh file.
    """
    tags: Dict[str, int] = {}

    with open(filename) as file:
        reading_tags = False
        for line in file.readlines():
            if "$PhysicalNames" in line:
                reading_tags = True
                continue
            if "$EndPhysicalNames" in line:
                reading_tags = False
                break

            if reading_tags:
                data = line.split()
                if (len(data)) == 1:
                    continue

                name = data[2].strip('"')
                tag = int(data[1])
                tags[name] = tag

    return tags


def get_vertex_markers(m: Mesh, tags: Dict[str, int], order: int = 1) -> Function:
    """
    finding interface markers
    values are:
     +1 in interior
      0 on interface
     -1 in exterior
     -2 on exterior boundary
    """
    V = FunctionSpace(m, "Lagrange", order)
    markers_interior = Function(V, val=np.zeros(V.node_count), dtype=int)
    markers_exterior = Function(V, val=np.zeros(V.node_count), dtype=int)

    par_loop(
        ("{[i] : 0 <= i < f.dofs}", "f[i, 0] = 1"),
        dx(tags["interior"]),
        {"f": (markers_interior, RW)},
        is_loopy_kernel=True,
    )
    par_loop(
        ("{[i] : 0 <= i < f.dofs}", "f[i, 0] =  1"),
        dx(tags["exterior"]),
        {"f": (markers_exterior, RW)},
        is_loopy_kernel=True,
    )

    markers = Function(V, dtype=int, name=f"markers{order}")

    par_loop(
        ("{[i] : 0 <= i < f.dofs}", "f[i, 0] = interior[i, 0] - exterior[i, 0]"),
        dx(),
        {
            "f": (markers, RW),
            "interior": (markers_interior, READ),
            "exterior": (markers_exterior, READ),
        },
        is_loopy_kernel=True,
    )

    par_loop(
        ("{[i] : 0 <= i < f.dofs}", "f[i, 0] = -2"),
        ds(tags["boundary"]),
        {"f": (markers, RW)},
        is_loopy_kernel=True,
    )

    return markers


def update_coords(
    m: Mesh, tags: Dict[str, int], interface_proj: Callable[[np.ndarray], np.ndarray]
) -> None:
    markers = get_vertex_markers(m, tags)

    c = m.coordinates
    c_data = c.dat.data
    N = len(c_data)

    markers_data = markers.dat.data

    for i in range(N):
        x = c_data[i]
        Ix = markers_data[i]
        if Ix == 0:
            x = interface_proj(x)
        c_data[i] = x


def iso_mesh(
    mesh: Mesh,
    tags: Dict[str, int],
    order: int,
    interface_proj: Callable[[np.ndarray], np.ndarray],
) -> Mesh:
    """
    Construct an isoparametric mesh adapted to the interface defined tags.
    Interface projection is carried out using interface_proj function.

    TODO interface_proj isn't used at all :/
    """
    trace("setting up spaces and new functions...", printrank="all")
    # extract coordinates and function space
    coords1 = mesh.coordinates
    dim = mesh.geometric_dimension()

    # create holders for initial guess and final higher order coordinates
    Vc = VectorFunctionSpace(mesh, "Lagrange", order, dim=dim)
    coordsk = Function(Vc)
    coordsk.interpolate(coords1)
    new_coords = Function(Vc)
    trace("setting up spaces and new functions...DONE", printrank="all")

    # get markers on initial mesh vertex-wise
    markers1 = get_vertex_markers(mesh, tags, 1)

    # convert markers to doubles since that's what pyopt wants
    markers1_float = Function(FunctionSpace(mesh, "Lagrange", 1))
    # markers1_float.interpolate(markers1)
    par_loop(
        ("{[i] : 0 <= i < f.dofs}", "f[i, 0] = g[i, 0]"),
        dx,
        {"f": (markers1_float, WRITE), "g": (markers1, READ)},
        is_loopy_kernel=True,
    )

    # construct loop
    func_dict = {
        "markers1": (markers1_float, READ),
    }
    for d in range(dim):
        func_dict[f"coords1_{d}"] = (coords1.sub(d), READ)
        func_dict[f"coordsk_{d}"] = (coordsk.sub(d), READ)
        func_dict[f"new_coords_{d}"] = (new_coords.sub(d), WRITE)

    code_headers = (
        f"#define ORDER {order}\n" + f"#define DIM {dim}\n" + "#define DEBUG 0\n"
    )
    code_body = open("utils/isomesh.c").read()
    code = code_headers + code_body

    # do the loop
    par_loop(code, dx(), func_dict)

    trace("loop done...", printrank="all")
    trace("waiting for barrier", printrank="all")

    ret = Mesh(new_coords)
    trace("constructiong new mesh...DONE", printrank="all")
    return ret


def mesh_h(mesh: Mesh) -> float:
    """
    Find mesh diameter.
    """
    V_const = FunctionSpace(mesh, "DG", degree=0)
    h_func = Function(V_const)
    dim = mesh.geometric_dimension()

    variables_dict = {"h_func": (h_func, MAX)}
    for d in range(dim):
        variables_dict[f"coords{d}"] = (mesh.coordinates.sub(d), READ)

    par_loop(
        f"""
        #define DIM {dim}
        """
        + r"""
        double h = 0.0;
        for(int i = 0; i < coords0.dofs; ++i) {
          for(int j = 0; j < i; ++j) {
             double h_ij2 = (coords0[i] - coords0[j])*(coords0[i] - coords0[j]);
        #if DIM > 1
             h_ij2 += (coords1[i] - coords1[j])*(coords1[i] - coords1[j]);
        #endif
        #if DIM > 2
             h_ij2 += (coords2[i] - coords2[j])*(coords2[i] - coords2[j]);
        #endif
             double h_ij = sqrt(h_ij2);
             if(h_ij > h) {
               h = h_ij;
             }
          }
        }

        h_func[0] = h;
        """,
        dx(),
        variables_dict,
    )

    with h_func.dat.vec_ro as h:
        return h.max()[1]
