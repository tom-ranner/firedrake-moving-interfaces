/* static void par_loop_kernel (double  coords1_0[3] , double  coords1_1[3] ,
 * double  coordsk_0[3] , double  coordsk_1[3] , double  markers1[3] , double
 * new_coords_0[3] , double  new_coords_1[3] ) */
{
#ifndef ORDER
#define ORDER 1
#endif
#ifndef DIM
#define DIM 3
#endif
#ifndef DEBUG
#define DEBUG 0
#endif

  int ORDER_factorial = 1;
  for (int i = 1; i <= ORDER; ++i) {
    ORDER_factorial *= i;
  }
  int DIM_factorial = 1;
  for (int i = 1; i <= DIM; ++i) {
    DIM_factorial *= i;
  }
  int ORDER_plus_DIM_factorial = 1;
  for (int i = 1; i <= DIM + ORDER; ++i) {
    ORDER_plus_DIM_factorial *= i;
  }

  const unsigned n_dofs1 = DIM + 1;
  const unsigned n_dofs =
      ORDER_plus_DIM_factorial / (ORDER_factorial * DIM_factorial);

#if DEBUG
  printf("====================\n");
  printf("n_dofs1: %d, ndofs: %d\n", n_dofs1, n_dofs);

  for (unsigned i = 0; i < n_dofs1; ++i) {
    printf("vertex[%d]: ", i);
    printf(" %f", coords1_0[i]);
#if DIM > 1
    printf(" %f", coords1_1[i]);
#endif
#if DIM > 2
    printf(" %f", coords1_2[i]);
#endif
    printf("\n");
  }
#endif

  unsigned on_interface_count = 0;
  const double on_interface_tol = 1.0e-14;
  for (unsigned i = 0; i < n_dofs1; ++i) {
    if (abs(markers1[i]) < on_interface_tol) {
      on_interface_count++;
    }
  }

  if (on_interface_count >= DIM + 1) {
    fprintf(stderr, "error: too many vertices on interface!\n");
    for (int i = 0; i < n_dofs1; ++i) {
      fprintf(stderr, "marker[%d] = %f, coords1[%d] =", i, markers1[i], i);

      double n = 0.0;
      fprintf(stderr, " %f", coords1_0[i]);
      n += coords1_0[i] * coords1_0[i];
#if DIM > 1
      fprintf(stderr, " %f", coords1_1[i]);
      n += coords1_1[i] * coords1_1[i];
#endif
#if DIM > 2
      fprintf(stderr, " %f", coords1_2[i]);
      n += coords1_2[i] * coords1_2[i];
#endif
      fprintf(stderr, "--> %f\n", n);
    }

    exit(9);
  }

#if DEBUG
  printf("on interface: %d\n", on_interface_count);
#endif

  if (on_interface_count > 1) {
    // transformation for barycentric coordinates
    double AK[DIM * DIM];
    double bK[DIM];
    for (unsigned i = 0; i < n_dofs1 - 1; ++i) {
      AK[0 * DIM + i] = coords1_0[i + 1] - coords1_0[0];
#if DIM > 1
      AK[1 * DIM + i] = coords1_1[i + 1] - coords1_1[0];
#endif
#if DIM > 2
      AK[2 * DIM + i] = coords1_2[i + 1] - coords1_2[0];
#endif
    }

    bK[0] = coords1_0[0];
#if DIM > 1
    bK[1] = coords1_1[0];
#endif
#if DIM > 2
    bK[2] = coords1_2[0];
#endif

#if DEBUG
    printf("AK:\n");
    for (unsigned i = 0; i < DIM; ++i) {
      for (unsigned j = 0; j < DIM; ++j) {
        printf(" %f", AK[i * DIM + j]);
      }
      printf("\n");
    }

    printf("bK:");
    for (unsigned i = 0; i < DIM; ++i) {
      printf(" %f", bK[i]);
    }
    printf("\n");
#endif

    // LU factorisation
    // from https://en.wikipedia.org/wiki/LU_decomposition

    // pivot matrix
    unsigned P[DIM + 1];
    {
      // initialise unit permutation matrix
      for (unsigned i = 0; i < DIM + 1; ++i) {
        P[i] = i;
      }

      for (unsigned i = 0; i < DIM; ++i) {
        // find max value
        double maxA = 0.0;
        unsigned imax = i;

        for (unsigned k = i; k < DIM; ++k) {
          if (fabs(AK[k * DIM + i]) > maxA) {
            maxA = fabs(AK[k * DIM + i]);
            imax = k;
          }
        }

        if (maxA < on_interface_tol) {
          printf("error! degenerate matrix!");
          exit(1);
        }

        // do pivoting
        if (imax != i) {
          // pivot P
          unsigned j = P[i];
          P[i] = P[imax];
          P[imax] = j;

          // pivot row of A
          for (unsigned j = 0; j < DIM; ++j) {
            double tmp = AK[i * DIM + j];
            AK[i * DIM + j] = AK[imax * DIM + j];
            AK[imax * DIM + j] = tmp;
          }

          // counting pivots starting from N (for determinant)
          P[DIM]++;
        }

        // do factorisation in place
        for (unsigned j = i + 1; j < DIM; ++j) {
          AK[j * DIM + i] /= AK[i * DIM + i];

          for (unsigned k = i + 1; k < DIM; ++k) {
            AK[j * DIM + k] -= AK[j * DIM + i] * AK[i * DIM + k];
          }
        }
      }
    }

#if DEBUG
    printf("LU:\n");
    for (unsigned i = 0; i < DIM; ++i) {
      for (unsigned j = 0; j < DIM; ++j) {
        printf(" %f", AK[i * DIM + j]);
      }
      printf("\n");
    }

    printf("P:");
    for (unsigned i = 0; i < DIM + 1; ++i) {
      printf(" %d", P[i]);
    }
    printf("\n");
#endif

    // for each dof
    for (unsigned i = 0; i < n_dofs; ++i) {
      // get old coord
      double my_c[DIM];
      my_c[0] = coordsk_0[i];
#if DIM > 1
      my_c[1] = coordsk_1[i];
#endif
#if DIM > 2
      my_c[2] = coordsk_2[i];
#endif

#if DEBUG
      printf("--------------------\n");
      printf("my_c:");
      for (unsigned d = 0; d < DIM; ++d) {
        printf(" %f", my_c[d]);
      }
      printf("\n");
#endif

      // find bary centric description
      double my_chat[DIM];
      {
        // LU factorisation solve
        // from https://en.wikipedia.org/wiki/LU_decomposition

        for (unsigned i = 0; i < DIM; ++i) {
          my_chat[i] = my_c[P[i]] - bK[P[i]];

          for (unsigned k = 0; k < i; ++k) {
            my_chat[i] -= AK[i * DIM + k] * my_chat[k];
          }
        }

        for (int i = DIM - 1; i >= 0; --i) {
          for (unsigned k = i + 1; k < DIM; ++k) {
            my_chat[i] -= AK[i * DIM + k] * my_chat[k];
          }

          my_chat[i] /= AK[i * DIM + i];
        }
      }

#if DEBUG
      printf("my_chat:");
      for (unsigned i = 0; i < DIM; ++i) {
        printf(" %f", my_chat[i]);
      }
      printf("\n");
#endif

      double lambda[DIM + 1];
      double chat_sum = 0.0;
      for (unsigned j = 0; j < DIM; ++j) {
        lambda[j + 1] = my_chat[j];
        chat_sum += my_chat[j];

        if (lambda[j + 1] < -on_interface_tol ||
            lambda[j + 1] > 1.0 + on_interface_tol) {
          printf("warning: bad lambda[%d] = %e (1-%e)\n", j + 1, lambda[j + 1], 1-lambda[j+1]);
        }
      }
      lambda[0] = 1.0 - chat_sum;

      if (lambda[0] < -on_interface_tol || lambda[0] > 1.0 + on_interface_tol) {
	printf("warning: bad lambda[%d] = %e (1-%e)\n", 0, lambda[0], 1-lambda[0]);
      }

#if DEBUG
      printf("lambda:");
      for (unsigned j = 0; j < DIM+1; ++j) {
        printf(" %f", lambda[j]);
      }
      printf("\n");
#endif

      double lambda_star = 0.0;
      for (unsigned j = 0; j < n_dofs1; ++j) {
        if (abs(markers1[j]) < on_interface_tol) {
          lambda_star += lambda[j];
        }
      }

#if DEBUG
      printf("lambda_star: %f\n", lambda_star);
#endif

      // if not at singlular point, update my_c
      if (lambda_star > on_interface_tol) {
        // project my_c to discrete interface
        double y[DIM];
        for (unsigned d = 0; d < DIM; ++d) {
          y[d] = 0.0;
        }

        for (unsigned i = 0; i < n_dofs1; ++i) {
          if (abs(markers1[i]) < on_interface_tol) {
	    y[0] += (lambda[i] / lambda_star) * coords1_0[i];
#if DIM > 1
	    y[1] += (lambda[i] / lambda_star) * coords1_1[i];
#endif
#if DIM > 2
	    y[2] += (lambda[i] / lambda_star) * coords1_2[i];
#endif
          }
        }

#if DEBUG
        printf("y:");
        for (unsigned d = 0; d < DIM; ++d) {
          printf(" %f", y[d]);
        }
        printf("\n");
#endif

        // project y to continuous interface
        // TODO only works for sphere/circle radius 0.5
        double p[DIM];

        double norm = 0.0;
        for (unsigned d = 0; d < DIM; ++d) {
          norm += y[d] * y[d];
        }
        norm = sqrt(norm);

        for (unsigned d = 0; d < DIM; ++d) {
          p[d] = 0.5 * y[d] / norm;
        }

#if DEBUG
        printf("p:");
        for (unsigned d = 0; d < DIM; ++d) {
          printf(" %f", p[d]);
        }
        printf("\n");
#endif

        // update coordinate
        for (unsigned d = 0; d < DIM; ++d) {
          my_c[d] += pow(lambda_star, ORDER + 2) * (p[d] - y[d]);
        }
      }

#if DEBUG
      printf("new c:");
      for (unsigned d = 0; d < DIM; ++d) {
        printf(" %f", my_c[d]);
      }
      printf("\n");
#endif

      new_coords_0[i] = my_c[0];
#if DIM > 1
      new_coords_1[i] = my_c[1];
#endif
#if DIM > 2
      new_coords_2[i] = my_c[2];
#endif
    }

#if DEBUG
    getchar();
#endif
  } else {
    for (unsigned int i = 0; i < n_dofs; ++i) {
      new_coords_0[i] = coordsk_0[i];
#if DIM > 1
      new_coords_1[i] = coordsk_1[i];
#endif
#if DIM > 2
      new_coords_2[i] = coordsk_2[i];
#endif
    }
  }
}
