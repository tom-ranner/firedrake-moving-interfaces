import sympy
from sympy.abc import l, t, z

bdf_coeffs = {}
for k in range(1, 7):
    bdf_poly = sympy.Poly(sympy.Sum(1 / l * (1 - z) ** l, (l, 1, k)).doit())
    bdf_coeffs[k] = [c.evalf() for c in bdf_poly.all_coeffs()]
    bdf_coeffs[k].reverse()
