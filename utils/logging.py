import sys
from enum import IntEnum
from typing import Union

from firedrake import COMM_WORLD
from firedrake.petsc import PETSc


class Log_level(IntEnum):
    NONE = 0
    WARNING = 1
    INFO = 2
    TRACE = 3


LOG_LEVEL = Log_level.INFO


def set_log_level(level: Union[int, Log_level]):
    global LOG_LEVEL
    LOG_LEVEL = level
    trace(f"LOG_LEVEL set to {LOG_LEVEL}")


def pprint(*args, **kwargs):
    if "printrank" in kwargs:
        printrank = kwargs["printrank"]
        kwargs.pop("printrank")
    else:
        printrank = 0

    if printrank == "all":
        print(f"[{COMM_WORLD.rank}] ", *args, **kwargs)
    else:
        if COMM_WORLD.rank == printrank:
            print(*args, **kwargs)


def log(*args, **kwargs):
    assert "level" in kwargs
    level = kwargs["level"]
    kwargs.pop("level")
    if level <= LOG_LEVEL:
        pprint(*args, **kwargs)
    sys.stdout.flush()


def trace(*args, **kwargs):
    log("###", *args, **kwargs, level=Log_level.TRACE)


def info(*args, **kwargs):
    log("##", *args, **kwargs, level=Log_level.INFO)


def warning(*args, **kwargs):
    log("#", *args, **kwargs, level=Log_level.WARNING)
