import sys
from typing import Optional

import numpy as np
from firedrake import (
    Constant,
    File,
    Function,
    FunctionSpace,
    Mesh,
    MeshHierarchy,
    SpatialCoordinate,
    as_vector,
    assemble,
    dS,
    ds,
    dx,
    sqrt,
)

from utils.logging import pprint, trace, warning
from utils.parameters import Parameters
from utils.utils import (
    get_mesh_tags,
    get_vertex_markers,
    iso_mesh,
    mesh_h,
    update_coords,
)


def eoc(e: float, e_old: float) -> Optional[float]:
    if e_old > 1.0e-16:
        return np.log(e / e_old) / np.log(0.5)
    return ""


alpha, beta = 1.0, 1.1


def X_func(x: np.ndarray, t: float = 0.0) -> np.ndarray:
    def p(y):
        p0 = np.prod([1 - yy * yy for yy in y])
        p1 = sum([yy * yy for yy in y]) ** (1.0 / 3.0)
        return p0 * p1

    r0 = sqrt(sum([xx * xx for xx in x])) / 0.5
    x2 = x / r0
    dim = len(x)
    d = as_vector([w * xx for w, xx in zip([alpha - 1.0, beta - 1.0, 0.0][:dim], x)])

    return x + d * p(x) / p(x2)


# get parameters
trace("reading parameters...")
parameters = Parameters(sys.argv[1:])
meshfile: str = parameters["meshfile"]
errorfile: str = parameters["errorfile"]
order: int = parameters["order"]
repeats: int = parameters["repeats"]
output: bool = parameters["output"]
trace("reading parameters...DONE")

trace("reading base mesh and tags...")
# create base mesh
base_mesh = Mesh(meshfile)
# get tags from mesh file
tags = get_mesh_tags(meshfile)
trace("reading base mesh and tags...DONE")

# TODO this isn't used any more
#      how to include into c code?
def interface_proj(x: np.ndarray) -> np.ndarray:
    return 0.5 * x / np.linalg.norm(x)


# do boundary projection
trace("updating coordinates...")
update_coords(base_mesh, tags, interface_proj)
trace("updating coordinates...DONE")

# true answers
dim = base_mesh.geometric_dimension()
if dim == 2:
    from scipy.special import ellipe

    a = max(alpha, beta) / 2.0
    b = min(alpha, beta) / 2.0
    e_sq = 1.0 - b ** 2 / a ** 2

    exact_I = a * b * np.pi
    exact_E = 2.0 ** dim - exact_I
    exact_G = 4.0 * a * ellipe(e_sq)
    exact_B = 8.0
elif dim == 3:

    def surface_area_ellipsoid(a, b, c):
        from scipy.special import ellipeinc, ellipkinc

        phi = np.arccos(c / a)
        m = (a ** 2 * (b ** 2 - c ** 2)) / (b ** 2 * (a ** 2 - c ** 2))
        temp = (
            ellipeinc(phi, m) * np.sin(phi) ** 2 + ellipkinc(phi, m) * np.cos(phi) ** 2
        )
        ellipsoid_area = 2.0 * np.pi * (c ** 2 + a * b * temp / np.sin(phi))
        return ellipsoid_area

    a = max(alpha, beta) / 2.0
    b = min(alpha, beta) / 2.0
    c = 1.0 / 2.0

    exact_I = (4.0 / 3.0) * np.pi * a * b * c
    exact_E = 2.0 ** dim - exact_I
    exact_G = surface_area_ellipsoid(a, b, c)
    exact_B = 6.0 * 2.0 ** 2

    print(a, b, c, exact_I)
else:
    raise ValueError

eI_old, eE_old, eG_old, eB_old, eAll_old = (0.0, 0.0, 0.0, 0.0, 0.0)
error_file = open(errorfile, "w")

for i in range(repeats):
    trace(f"starting level {i}")

    trace("generating iso_mesh...")
    mesh = iso_mesh(base_mesh, tags, order, interface_proj)
    coords0_ptr = mesh.coordinates.dat.data.copy()
    coords_ptr = mesh.coordinates.dat.data
    trace("generating iso_mesh...DONE")

    trace("updating coordinates...")
    for j in range(len(coords0_ptr)):
        coords_ptr[j] = X_func(coords0_ptr[j])
    trace("updating coordinates...DONE")

    trace("finding mesh_h...")
    h = mesh_h(mesh)
    trace("finding mesh_h...DONE")

    trace("performing integrals...")
    I = assemble(Constant(1) * dx(tags["interior"], domain=mesh))
    E = assemble(Constant(1) * dx(tags["exterior"], domain=mesh))

    G = assemble(Constant(1) * dS(tags["interface"], domain=mesh))
    B = assemble(Constant(1) * ds(tags["boundary"], domain=mesh))
    trace("performing integrals...DONE")

    eI = abs(I - exact_I)
    eE = abs(E - exact_E)
    eG = abs(G - exact_G)
    eB = abs(B - exact_B)
    eAll = eI + eE + eG + eB

    pprint(
        f"level = {i}\n\t",
        I,
        eI,
        eoc(eI, eI_old),
        "\n\t",
        E,
        eE,
        eoc(eE, eE_old),
        "\n\t",
        G,
        eG,
        eoc(eG, eG_old),
        "\n\t",
        B,
        eB,
        eoc(eB, eB_old),
        "\n\t",
        I + E,
    )

    trace("testing interface...")
    x = SpatialCoordinate(mesh)
    Phi = (
        sum([xj * xj / (wj * wj) for xj, wj in zip(x, [alpha, beta, 1][:dim])])
        - 0.5 ** 2
    )
    f = Function(FunctionSpace(mesh, "CG", order), name="phi")
    f.interpolate(Phi)
    eG2 = assemble(abs(f) * dS(tags["interface"], domain=mesh))
    # assert eG2 < 1.0e-16
    if eG2 > 1.0e-16:
        warning("eG2:", eG2)
    trace("testing interface...DONE")

    if output:
        markers = get_vertex_markers(mesh, tags, order)
        n_interface_markers = len([m for m in markers.dat.data if m == 0])
        print(f"{n_interface_markers}")

        File(f"output/test_{order}_{i}.pvd").write(markers, f)
        pprint(f"written to output/test_{order}_{i}.pvd")

    # if I + E > 2.0 ** dim + 1.0e-8:
    #     break

    error_file.write(f"level{i}:\n")
    error_file.write(f"  h: {h}\n")
    error_file.write(f"  eI: {eI}\n")
    error_file.write(f"  eE: {eE}\n")
    error_file.write(f"  eG: {eG}\n")
    error_file.write(f"  eG2: {eG2}\n")
    error_file.write(f"  eB: {eB}\n")
    error_file.write(f"  e_all: {eAll}\n")

    if repeats > 1:
        base_mesh = MeshHierarchy(base_mesh, 1)[-1]
        update_coords(base_mesh, tags, interface_proj)

        eI_old, eE_old, eG_old, eB_old, eAll_old = (eI, eE, eG, eB, eAll)
