import os.path
import re
import sys
from typing import List, Optional

import numpy as np
import pandas as pd
from matplotlib import pyplot as plt
from matplotlib import rcParams
from rotation import RotationAwareAnnotation2


def get_order(fn: str) -> Optional[int]:
    p = re.compile("order[0-9]")
    result = p.search(fn)
    if result is None:
        return None
    return int(result.group(0)[-1])


displayed_slopes = []


def loglog_with_fit(
    ax, x: np.ndarray, y_list: List[np.ndarray], order: Optional[int] = None
):
    # choose colour
    colour = next(ax._get_lines.prop_cycler)["color"]

    if order is None:
        label = None
    else:
        label = f"$k = {order}$"

    MARKERS = ".+x1234"[: len(y_list)]
    LINESTYLES = ["solid", "dashed", "dotted", "dashdot"][: len(y_list)]

    for y, marker, linestyle in zip(y_list, MARKERS, LINESTYLES):
        x_ = np.array([xx for xx, yy in zip(x, y) if xx > 0 and yy > 0])
        y_ = np.array([yy for xx, yy in zip(x, y) if xx > 0 and yy > 0])
        x, y = x_, y_

        if len(x) == 0 and len(y) == 0:
            print(f"no positive data, skipping {label}")
            return

        # plot raw
        ax.plot(x, y, marker=marker, color=colour, label=label, linestyle="")

        # fit loglog
        min_err = 1e6
        m, logbm = 0, [0]
        for m_test in range(1, 10):
            ret = np.polyfit(
                np.log(x[len(x) // 2 :]),
                np.log(y[len(y) // 2 :]) / m_test - np.log(x[len(x) // 2 :]),
                deg=0,
                full=True,
            )

            logbm_test = ret[0]
            err = ret[1][0]

            print(m_test, logbm, err)

            if err < min_err:
                m = m_test
                logbm = logbm_test
                min_err = err

        b = np.exp(m * logbm[0])

        print(f"{label} has fit error ~ {b} * h ** {m}")
        slope_str = f"slope {m:d}"

        # plot fit
        xx = np.linspace(x.min(), x.max())
        yy = b * xx ** m
        ax.plot(xx, yy, color=colour, linestyle=linestyle)

        if m not in displayed_slopes:
            # plot text along line
            RotationAwareAnnotation2(
                slope_str,
                xy=(xx[0], yy[0]),
                p=(xx[1], yy[1]),
                ax=ax,
                xytext=(4, 5),
                textcoords="offset points",
                color=colour,
            )
            displayed_slopes.append(m)

        # one label per call
        label = None


if __name__ == "__main__":
    # read file names
    outfile = sys.argv[1]
    infiles = sys.argv[2:]

    params = os.path.splitext(outfile)[0].split("-")
    value_label_map = {
        "h": r"$h$",
        "dt": r"$\tau$",
        "n_dofs": "dofs",
        "l2_error": r"$L^2$ error",
        "h1_error": r"$H^1$ error",
        "e_all": r"All errors ($e_I, e_E, e_G, e_B$)",
        "eI": r"Volume error ($\Omega_I$)",
        "eE": r"Volume error ($\Omega_E$)",
        "eG": r"Interface area error ($\Gamma$)",
        "eB": r"Boundary area error ($\partial \Omega$)",
    }

    xkey = params[-2]
    ykey = params[-1].split(":")

    xlabel = value_label_map[xkey]
    ylabel = [value_label_map[ykey_] for ykey_ in ykey]

    # set parameters
    rcParams["axes.labelsize"] = 8
    rcParams["xtick.labelsize"] = 8
    rcParams["ytick.labelsize"] = 8
    rcParams["legend.fontsize"] = 8
    rcParams["font.family"] = "serif"
    rcParams["font.serif"] = ["Computer Modern Roman"]
    rcParams["text.usetex"] = True

    # make figure
    width_pt = 360.0 * 0.8
    width_inch = width_pt / 72.0  # 72 pts per inch
    ratio = 0.5 * (1.0 + np.sqrt(5.0))
    height_inch = width_inch / ratio
    fig = plt.figure(figsize=(width_inch, height_inch))
    ax = fig.subplots()
    ax.set_xscale("log")
    ax.set_yscale("log")
    # fig = plt.figure()

    # read data and plot
    for infile in infiles:
        try:
            data = pd.read_csv(infile)
        except pd.errors.EmptyDataError:
            continue
        order = get_order(infile)
        loglog_with_fit(ax, data[xkey], [data[ykey_] for ykey_ in ykey], order)

    # set labels
    plt.legend()
    plt.xlabel(xlabel)
    if len(ylabel) == 1:
        plt.ylabel(ylabel[0])
    else:
        plt.ylabel("Error")

    # save output
    fig.tight_layout(pad=0.15)  # Make the figure use all available whitespace
    plt.savefig(outfile)
