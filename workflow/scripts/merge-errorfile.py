import os
import sys
from typing import Union

import numpy as np
import pandas as pd
import yaml

# read file names
outfile = sys.argv[1]
infiles = sorted(sys.argv[2:])

# read data and concat
data = []
for fn in infiles:
    print(f"loading data from {fn}")
    with open(fn, "r") as f:
        my_data = yaml.safe_load(f)
        try:
            for values in my_data.values():
                data.append(values)
        except AttributeError:
            print(f"{fn} empty")

print(data)
data = pd.json_normalize(data)

# compute eocs
for label in data.columns:
    if "error" not in label:
        continue
    data[f"eoc({label})"] = np.array(
        [np.NaN]
        + [
            np.log(e / e_old) / np.log(h / h_old)
            for e, e_old, h, h_old in zip(
                data[label][1:].astype(np.float64),
                data[label][:-1].astype(np.float64),
                data["h"][1:].astype(np.float64),
                data[r"h"][:-1].astype(np.float64),
            )
        ]
    )

# determine extension
ext = os.path.splitext(outfile)[-1]
if ext == ".csv":
    data.to_csv(outfile, index=False)
elif ext == ".tex":

    if data.empty:
        open(outfile, "w").close()
    else:

        del data["n_dofs"]
        column_map = {
            "h": r"$h$",
            "dt": r"$\tau$",
            "n_dofs": r"$N$",
            "l2_error": r"$L^2$ error",
            "h1_error": r"$H^1$ error",
        }
        columns = list(data.columns.copy())
        for idx, c in enumerate(columns):
            for raw, tex in column_map.items():
                if raw in c:
                    columns[idx] = c.replace(raw, tex)

        def sc_math(f: Union[float, str]) -> str:
            if type(f) == str:
                return f
            format_f = f"{f:.5e}"
            m, e = format_f.split("e")
            e_int = int(e)
            if e_int != 0:
                s = r"$" + m + r" \times 10^{" + str(e_int) + r"}$"
            else:
                s = r"$" + m + r"$"
            return s

        data.to_latex(
            outfile,
            header=columns,
            index=False,
            na_rep="---",
            escape=False,
            formatters={
                "h": sc_math,
                "dt": sc_math,
                "l2_error": sc_math,
                "h1_error": sc_math,
            },
            column_format="c" * len(columns),
        )
else:
    raise ValueError(f"unknown extension: {ext}")
