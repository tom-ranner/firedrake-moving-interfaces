As a first simple test case, we study an elliptic interface problem.
Consider a domain :math:`\Omega \subset {{ dimension }}` split into two subdomains :math:`\Omega^\pm`.
The problem we wish to consider is:

  Let :math:`f \in L^2(\Omega)`, :math:`g \in H^{1/2}(\Gamma)` and :math:`D \colon \Omega \to \mathbb{R}` with :math:`D|_{\Omega^{\pm}}` continuous. Find :math:`u \colon \Omega \to \mathbb{R}` such that

  .. math::
    \begin{align*}
    - \nabla \cdot (D \nabla u) & = f && \mbox{ in } \Omega^{\pm} \\
    \left[\frac{\partial u}{\partial \nu}\right] & = g && \mbox{ on } \Gamma \\
    u & = 0 && \mbox{ on } \partial \Omega.
    \end{align*}

Let :math:`\Omega = [-1,1]^{{dimension}}`, :math:`\Omega^+ = B(0, 1/2)` (ball radius :math:`1/2` centred at 0).
We note that the interface :math:`\Gamma` can be described by the level set function :math:`\Phi(x) = |x|^2 - 1/4`.
We set :math:`D|_{\Omega^+} = 10` and :math:`D|_{\Omega^-} = 1`.
We construct :math:`f` and :math:`g` such that the exact solution :math:`u` is given by

.. math::
  u(x) =
  |\Phi(x)| \prod_{i=1}^{{dimension}} (x_i^2 - 1) \cos(4 x_i).

We compute using isoparametric elements of order 1, 2, 3 on a fixed mesh to find a solution :math:`u_h`.
For elements of order :math:`k` we expect convergence of order :math:`k+1` for the error :math:`u - u_h` in the :math:`L^2(\Omega)` norm.

