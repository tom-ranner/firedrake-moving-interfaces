All numerical results are computed using the firedrake package\cite{petsc-user-ref,petsc-efficient,Dalcin2011,Rathgeber2016}
% TODO: best practice would put this as a zenodo based doi
Simulation codes are available at
\begin{quote}
  \url{https://gitlab.com/tom-ranner/firedrake-moving-interfaces}
\end{quote}
Results are computed on a sequence of meshes generated using GMSH\cite{GMSH} rather than successive refinement.

