def get_threads(filename: str, degree: int) -> int:
    """
    Compute number of threads to use based on estimate of number of degrees of freedom.
    https://www.firedrakeproject.org/parallelism.html#id5
    """
    return 1
    import subprocess

    cmd = [
        "./firedrake-python",
        "dof-estimator.py",
        f"--mesh={filename}",
        f"--order={degree}",
    ]
    result = subprocess.run(cmd, capture_output=True)
    dofs_str = result.stdout.decode("utf-8")
    dofs = int(dofs_str)
    threads = int(1 + dofs // dofs_per_thread)
    return threads
