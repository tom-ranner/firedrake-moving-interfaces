import sys

import numpy as np
from firedrake import (
    COMM_WORLD,
    Constant,
    DirichletBC,
    File,
    Function,
    FunctionSpace,
    Mesh,
    MeshHierarchy,
    SpatialCoordinate,
    TestFunction,
    TrialFunction,
    assemble,
    conditional,
    div,
    dot,
    dS,
    dx,
    ge,
    grad,
    sin,
    solve,
    sqrt,
)

from utils.logging import info, pprint, trace, warning
from utils.parameters import Parameters
from utils.utils import (
    get_mesh_tags,
    get_vertex_markers,
    iso_mesh,
    mesh_h,
    update_coords,
)

# from utils.logging import set_log_level

# set_log_level(3)

trace(f"running with COMM_WORLD.size = {COMM_WORLD.size}")

# parameters
trace("reading parameters...")
parameters = Parameters(sys.argv[1:])
meshfile: str = parameters["meshfile"]
errorfile: str = parameters["errorfile"]
order: int = parameters["order"]
repeats: int = parameters["repeats"]
output: bool = parameters["output"]
trace("reading parameters...DONE")

trace("defining exact solution and data...")


# construct exact solution
def uI_func(x: SpatialCoordinate):
    u1 = Phi(x)
    u3 = np.prod([sin(2.0 * np.pi * xx) for xx in x])
    return u1 * u3


def uE_func(x: SpatialCoordinate):
    return -uI_func(x)


def u_func(x: SpatialCoordinate):
    return conditional(ge(Phi(x), 0), uE_func(x), uI_func(x))


# problem data
def A_I(x: SpatialCoordinate) -> float:
    return 10.0


def A_E(x: SpatialCoordinate) -> float:
    return 1.0


def B_I(x: SpatialCoordinate):
    return 5 * grad(x[0])


def B_E(x: SpatialCoordinate):
    return -5 * grad(x[0])


def C_I(x: SpatialCoordinate) -> float:
    return 1.0


def C_E(x: SpatialCoordinate) -> float:
    return 10.0


def fI_func(x: SpatialCoordinate):
    u = uI_func(x)
    return -A_I(x) * div(grad(u)) + dot(B_I(x), grad(u)) + C_I(x) * u


def fE_func(x: SpatialCoordinate):
    u = uE_func(x)
    return -A_E(x) * div(grad(u)) + dot(B_E(x), grad(u)) + C_E(x) * u


def g_func(x: SpatialCoordinate):
    u_I = uI_func(x)
    du_I = grad(u_I)

    u_E = uE_func(x)
    du_E = grad(u_E)

    phi = Phi(x)
    nu_tilde = grad(phi)
    nu = nu_tilde / sqrt(dot(nu_tilde, nu_tilde))

    return A_I(x) * np.dot(du_I, nu) - A_E(x) * np.dot(du_E, nu)


trace("defining exact solution and data...DONE")

trace("creating base_mesh and reading tags...")

# create base mesh
base_mesh = Mesh(meshfile)

# get tags from mesh file
tags = get_mesh_tags(meshfile)


# interface information
def Phi(x: SpatialCoordinate) -> float:
    return dot(x, x) - 0.25


def interface_proj(x: np.ndarray) -> np.ndarray:
    return 0.5 * x / np.linalg.norm(x)


trace("creating base_mesh and reading tags...DONE")

# do boundary projection
trace("doing interface projection...")
update_coords(base_mesh, tags, interface_proj)
trace("doing interface projection...DONE")
error_file = open(errorfile, "w")

old_h, old_l2error, old_h1error = -1, -1, -1

for i in range(repeats):
    trace(f"starting level {i}")

    # generate isoparameter mesh
    trace("generating isoparametric mesh...")
    mesh = iso_mesh(base_mesh, tags, order, interface_proj)
    x = SpatialCoordinate(mesh)
    trace("generating isoparametric mesh...DONE")

    trace("discretisaing problem...")
    # function spaces and test functions
    V = FunctionSpace(mesh, "Lagrange", order)
    u = TrialFunction(V)
    v = TestFunction(V)

    # create forcing functions
    x = SpatialCoordinate(mesh)
    fI = Function(V, name="fI")
    fI.interpolate(fI_func(x))
    fE = Function(V, name="fE")
    fE.interpolate(fE_func(x))
    g = Function(V, name="g")
    g.interpolate(g_func(x))

    # variational form
    a = (
        A_I(x) * dot(grad(u), grad(v)) + dot(B_I(x), grad(u)) * v + C_I(x) * u * v
    ) * dx(tags["interior"]) + (
        A_E(x) * dot(grad(u), grad(v)) + dot(B_E(x), grad(u)) * v + C_E(x) * u * v
    ) * dx(
        tags["exterior"]
    )
    L = (
        fI * v * dx(tags["interior"])
        + fE * v * dx(tags["exterior"])
        + g * v("-") * dS(tags["interface"])
    )

    # boundary conditions
    DirBC = DirichletBC(V, 0, [tags["boundary"]])
    trace("discretisaing problem...DONE")

    # solve
    trace("solving...")
    u = Function(V, name="solution")
    solve(
        a == L,
        u,
        DirBC,
        solver_parameters={
            "atol": 1.0e-15,
            "rtol": 1.0e-15,
            # "ksp_type": "cg",
            # "ksp_max_it": max(10000, 10 * V.node_count),
            # "pc_type": "hypre",
            # "pc_hypre_type": "boomeramg",
        },
    )
    # u.interpolate(u_func(x))
    trace("solving...DONE")

    trace("interpolating exact...")
    V_exact = FunctionSpace(mesh, "Lagrange", order + 2)
    u_exact = Function(V_exact)
    u_exact.interpolate(u_func(x))
    trace("interpolating exact...DONE")

    # plot
    if output:
        trace("outputting plotting data...")
        u_exactI = Function(V_exact, name="exact")
        u_exactI.interpolate(u_func(x))
        PhiI = Function(V, name="phi")
        PhiI.interpolate(Phi(x))
        File(f"output/elliptic_{i}.pvd").write(
            u,
            u_exactI,
            fI,
            fE,
            g,
            PhiI,
            get_vertex_markers(mesh, tags, order),
        )
        info(f"ouput written to output/elliptic_{i}.pvd")
        trace("outputting plotting data...DONE")
    else:
        trace("outputting plotting data...SKIPPED")

    # compute error
    trace("computing mesh size and finding dofs...")
    h = mesh_h(mesh)
    n_dofs = V.dim()
    trace("computing mesh size and finding dofs...DONE")
    trace("computing errors...")
    l2error2 = assemble((u - u_exact) ** 2 * dx())
    l2error = sqrt(l2error2)
    h1error2 = assemble(dot(grad(u - u_exact), grad(u - u_exact)) * dx())
    h1error = sqrt(h1error2)
    trace("computing errors...DONE")

    trace("outputting errors...")
    if old_h < 0.0:
        pprint(f"{h=} {n_dofs=}")
        pprint(f"{l2error=}")
        pprint(f"{h1error=}")
    else:
        pprint(f"{h=} {n_dofs=}")
        eoc_l2, eoc_h1 = None, None
        if l2error > 1.0e-16 and old_l2error > 1.0e-16:
            eoc_l2 = np.log(l2error / old_l2error) / np.log(h / old_h)
        if h1error > 1.0e-16 and old_h1error > 1.0e-16:
            eoc_h1 = np.log(h1error / old_h1error) / np.log(h / old_h)
        pprint(f"{l2error=} {eoc_l2=}")
        pprint(f"{h1error=} {eoc_h1=}")

    error_file.write(f"level{i}:\n")
    error_file.write(f"  h: {h}\n")
    error_file.write(f"  n_dofs: {n_dofs}\n")
    error_file.write(f"  l2_error: {l2error}\n")
    error_file.write(f"  h1_error: {h1error}\n")
    trace("outputting errors...DONE")

    if COMM_WORLD.size == 1:
        trace("refining mesh...")
        base_mesh = MeshHierarchy(base_mesh, 1)[-1]
        update_coords(base_mesh, tags, interface_proj)
        trace("refining mesh...DONE")
    elif repeats > 1:
        warning("unable to refine in parallel")

    trace("checking refinement...")
    vol = assemble(Constant(1.0) * dx(domain=mesh))
    dim = x.ufl_shape[0]
    if vol > 2.0 ** dim + 1.0e-8:
        pprint("unable to continue refinement")
        break
    trace("checking refinement...DONE")

    old_h = h
    old_l2error = l2error
    old_h1error = h1error
