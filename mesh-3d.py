import sys
from typing import List, Tuple

import gmsh
import numpy as np

L = int(sys.argv[1])
gmsh.initialize()

# parameters
R = 0.5  # radius

# mesh size
lc = 2.0 ** -L

gmsh.model.add(f"t{L}-3d")

# interface - coarse
pts = [
    np.array([R, 0, 0]),  # 0
    np.array([0, R, 0]),  # 1
    np.array([0, 0, R]),  # 2
    np.array([0, -R, 0]),  # 3
    np.array([0, 0, -R]),  # 4
    np.array([-R, 0, 0]),
]  # 5
tris = [
    [0, 1, 2],
    [0, 2, 3],
    [0, 3, 4],
    [0, 4, 1],
    [5, 1, 2],
    [5, 2, 3],
    [5, 3, 4],
    [5, 4, 1],
]


def refine(
    pts: List[np.ndarray], tris: List[List[int]]
) -> Tuple[List[np.ndarray], List[List[int]]]:
    """
    Refine a coarse triangulation of the interface mesh.
    """
    new_tris: List[List[int]] = []
    for tri in tris:
        mid_pts_idx = []
        for i in range(3):
            A = pts[tri[i]]
            B = pts[tri[(i + 1) % 3]]
            pt_ = (A + B) / 2.0
            pt = R * pt_ / np.linalg.norm(pt_)

            found = False
            for o_idx, o_pt in enumerate(pts):
                if np.linalg.norm(pt - o_pt) < 1.0e-12:
                    mid_pts_idx.append(o_idx)
                    found = True
                    break
            if not found:
                pts.append(pt)
                mid_pts_idx.append(len(pts) - 1)

        new_tri_pts_0 = [tri[0], mid_pts_idx[0], mid_pts_idx[2]]
        new_tri_pts_1 = [tri[1], mid_pts_idx[1], mid_pts_idx[0]]
        new_tri_pts_2 = [tri[2], mid_pts_idx[2], mid_pts_idx[1]]
        new_tri_pts_3 = mid_pts_idx

        new_tris.append(new_tri_pts_0)
        new_tris.append(new_tri_pts_1)
        new_tris.append(new_tri_pts_2)
        new_tris.append(new_tri_pts_3)

    return pts, new_tris


# do refinement
for l in range(L):
    print("refinement:", l)
    pts, tris = refine(pts, tris)
    print(" n pts:", len(pts), "n tris:", len(tris))

# build lines and loops
lines = []
loops = []
signs = []
for tri in tris:
    my_lines = [[tri[0], tri[1]], [tri[1], tri[2]], [tri[2], tri[0]]]

    loop = []
    sign = []
    for line in my_lines:
        # try forwards
        try:
            l = lines.index(line)
            s = 1
        except ValueError:
            try:
                l = lines.index([line[1], line[0]])
                s = -1
            except ValueError:
                if line[0] < line[1]:
                    lines.append(line)
                    l = len(lines) - 1
                    s = 1
                elif line[1] < line[0]:
                    lines.append([line[1], line[0]])
                    l = len(lines) - 1
                    s = -1
                else:
                    print("lines should have different indicies")
                    raise ValueError

        loop.append(l)
        sign.append(s)
    loops.append(loop)
    signs.append(sign)

# find mesh diameter
lc = 0.0
for tri in tris:
    tri_pts = [pts[t] for t in tri]
    n = len(tri_pts)
    for i in range(n):
        for j in range(i):
            h_ij = np.linalg.norm(tri_pts[i] - tri_pts[j])
            lc = max(h_ij, lc)

print(f"mesh parameter {lc}")

# points around cube
gmsh.model.geo.addPoint(1, 1, 1, lc, 1)
gmsh.model.geo.addPoint(-1, 1, 1, lc, 2)
gmsh.model.geo.addPoint(-1, -1, 1, lc, 3)
gmsh.model.geo.addPoint(1, -1, 1, lc, 4)
gmsh.model.geo.addPoint(1, 1, -1, lc, 5)
gmsh.model.geo.addPoint(-1, 1, -1, lc, 6)
gmsh.model.geo.addPoint(-1, -1, -1, lc, 7)
gmsh.model.geo.addPoint(1, -1, -1, lc, 8)

# lines around square
gmsh.model.geo.addLine(1, 2, 1)
gmsh.model.geo.addLine(2, 3, 2)
gmsh.model.geo.addLine(3, 4, 3)
gmsh.model.geo.addLine(4, 1, 4)

gmsh.model.geo.addLine(5, 6, 5)
gmsh.model.geo.addLine(6, 7, 6)
gmsh.model.geo.addLine(7, 8, 7)
gmsh.model.geo.addLine(8, 5, 8)

gmsh.model.geo.addLine(1, 5, 9)
gmsh.model.geo.addLine(2, 6, 10)
gmsh.model.geo.addLine(3, 7, 11)
gmsh.model.geo.addLine(4, 8, 12)

# boundary loop
gmsh.model.geo.addCurveLoop([1, 2, 3, 4], 1)
gmsh.model.geo.addCurveLoop([5, 6, 7, 8], 2)
gmsh.model.geo.addCurveLoop([1, 10, -5, -9], 3)
gmsh.model.geo.addCurveLoop([2, 11, -6, -10], 4)
gmsh.model.geo.addCurveLoop([3, 12, -7, -11], 5)
gmsh.model.geo.addCurveLoop([4, 9, -8, -12], 6)

# boundary surfaces
boundary_surface_id = list(range(1, 7))
for i in boundary_surface_id:
    gmsh.model.geo.addPlaneSurface([i], i)
boundary = gmsh.model.geo.addSurfaceLoop(boundary_surface_id)

# assign to gmsh constructs
g_pts = [gmsh.model.geo.addPoint(p[0], p[1], p[2], lc) for p in pts]
g_lines = [gmsh.model.geo.addLine(g_pts[l[0]], g_pts[l[1]]) for l in lines]
g_loops = [
    gmsh.model.geo.addCurveLoop(
        [s[0] * g_lines[l[0]], s[1] * g_lines[l[1]], s[2] * g_lines[l[2]]]
    )
    for s, l in zip(signs, loops)
]

# interface surfaces
interface_surface_id = g_loops
# interface surfaces
for i in interface_surface_id:
    gmsh.model.geo.addPlaneSurface([i], i)
interface = gmsh.model.geo.addSurfaceLoop(interface_surface_id)

# 3d domains
interior = gmsh.model.geo.addVolume([interface])
exterior = gmsh.model.geo.addVolume([boundary, interface])

gmsh.model.geo.synchronize()

# physical tags
boundary_group = gmsh.model.addPhysicalGroup(2, boundary_surface_id)
interface_group = gmsh.model.addPhysicalGroup(2, interface_surface_id)
interior_group = gmsh.model.addPhysicalGroup(3, [interface])
exterior_group = gmsh.model.addPhysicalGroup(3, [boundary, interface])

gmsh.model.setPhysicalName(2, boundary_group, "boundary")
gmsh.model.setPhysicalName(2, interface_group, "interface")
gmsh.model.setPhysicalName(3, interior_group, "exterior")
gmsh.model.setPhysicalName(3, exterior_group, "interior")

# do the meshing
gmsh.model.mesh.generate(3)
gmsh.write(f"mesh/t{L}-3d.msh")

# visualise
if "--visualise" in sys.argv[1:]:
    gmsh.fltk.run()

# This should be called when you are done using the Gmsh Python API:
gmsh.finalize()
