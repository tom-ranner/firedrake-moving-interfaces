import sys

from firedrake import FunctionSpace, Mesh

from utils.logging import set_log_level
from utils.parameters import Parameters

# no logging
set_log_level(0)

# get parameters
parameters = Parameters(sys.argv[1:], verbose=False)
meshfile: str = parameters["meshfile"]
order: int = parameters["order"]

# generate mesh and function space of correct order
mesh = Mesh(meshfile)
V = FunctionSpace(mesh, "Lagrange", order)

# print number of nodes
print(V.node_count)
