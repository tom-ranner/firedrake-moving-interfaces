\documentclass{amsart}

\usepackage{graphicx,hyperref}
\usepackage{caption,subcaption,booktabs}

\usepackage{amsthm}
\newtheorem{problem}{Problem}
\newcommand{\R}{\mathbb{R}}

\usepackage[nameinlink]{cleveref}

\usepackage{verbatim}

\begin{document}
\section{Numerical results}

All numerical results are computed using the firedrake package\cite{petsc-user-ref,petsc-efficient,Dalcin2011,Rathgeber2016}
% TODO: best practice would put this as a zenodo based doi
Simulation codes are available at
\begin{quote}
  \url{https://gitlab.com/tom-ranner/firedrake-moving-interfaces}
\end{quote}
Results are computed on a sequence of meshes generated using GMSH\cite{GMSH} rather than successive refinement.

\subsection{Results for an elliptic problem}
% note: this section could/should problem be removed

As a first simple test case, we study an elliptic interface problem.
Suppose we are given a \emph{fixed}, bounded domain $\Omega \subset \R^d$, such that we have a subdomain $\Omega_1$ which has a boundary $\Gamma$, and a subdomain $\Omega_2 = \Omega \setminus \overline{\Omega}_1$.
We denote the outward pointing normal to $\Gamma$ by $\nu$.
The problem we wish to consider is:

\begin{problem}
  Let $f \in L^2(\Omega)$, $g \in H^{1/2}(\Gamma)$ and $A \colon \Omega \to \mathbb{R}$ with $A|_{\Omega_j}$ continuous for $j=1,2$. Find $u \colon \Omega \to \mathbb{R}$ such that
  %
\begin{align*}
  - \nabla \cdot (A \nabla u) & = f && \mbox{ in } \Omega^{\pm} \\
  \left[\frac{\partial u}{\partial \nu}\right] & = g && \mbox{ on } \Gamma \\
  u & = 0 && \mbox{ on } \partial \Omega.
\end{align*}
\end{problem}

For $d = 2, 3$, let $\Omega = [-1,1]^d$, $\Omega^+ = B(0, 1/2)$ (ball radius $1/2$ centred at 0).
We note that the interface $\Gamma$ can be described by the level set function $\Phi(x) = |x|^2 - 1/4$.
We set $A|_{\Omega_1} = 10$ and $A|_{\Omega_2} = 1$.
We construct $f$ and $g$ such that the exact solution $u$ is given by
\[
  u(x) =
  |\Phi(x)| \prod_{i=1}^d \sin(2 \pi x_i).
\]
We compute using isoparametric elements of order 1, 2, 3 on a fixed mesh to find a solution $u_h$.
For elements of order $k$ we expect convergence of order $k+1$ for the error $u - u_h$ in the $L^2(\Omega)$ norm.
The results are shown in \cref{fig:results-elliptic} for the cases $d=2,3$ respectively.
The precise numerical values are shown in \cref{tab:elliptic-2d,tab:elliptic-3d}.

\begin{figure}[tbh]
  \includegraphics{output/elliptic-2d-h-l2_error}
  \includegraphics{output/elliptic-3d-h-l2_error}
  \caption{$L^2$ error for elliptic problem for $d=2$ (left) and $d=3$ (right).}
  \label{fig:results-elliptic}
\end{figure}

\begin{table}[p]
  \begin{subtable}{\textwidth}
    \centering
    \include{output/elliptic-order1-2d.tex}
  \caption{Order 1, $d=2$}
\end{subtable}

\begin{subtable}{\textwidth}
  \centering
    \include{output/elliptic-order2-2d.tex}
  \caption{Order 2, $d=2$}
\end{subtable}

\begin{subtable}{\textwidth}
  \centering
    \include{output/elliptic-order3-2d.tex}
  \caption{Order 2, $d=2$}
\end{subtable}

\caption{Numerical results for the elliptic problem for $d=2$}
\label{tab:elliptic-2d}
\end{table}

\begin{table}[p]
\begin{subtable}{\textwidth}
  \centering
    \include{output/elliptic-order1-3d.tex}
  \caption{Order 1, $d=3$}
\end{subtable}

\begin{subtable}{\textwidth}
  \centering
    \include{output/elliptic-order2-3d.tex}
  \caption{Order 2, $d=3$}
\end{subtable}

\begin{subtable}{\textwidth}
  \centering
    \include{output/elliptic-order3-3d.tex}
  \caption{Order 3, $d=3$}
\end{subtable}

\caption{Results for elliptic problem for $d=3$.}
\label{tab:elliptic-3d}
\end{table}

\subsection{Time discretisation of advection-diffusion problem}

We start from the spatial discretisation from \cref{sec:??}. We will apply a backward time discretisation of order $q$.
We take a partition of the time interval $0 = t_0 < t_1 < \cdots < t_M = T$.
For simplicity we assume that each time interval is of the same length: $\tau := t_j - t_{j-1}$ for $j = 1, 2, \ldots, M$.

Our first task is to define our approach to constructing the evolving triangulation of $\Omega$.
We assume we are given an isoparametric approximation of the initial domain $\Omega$ subdivided into elements $\tilde{\mathcal{T}}$.
We assume the triangulation is ``fitted'' to $\Gamma_0$ in the sense that there is a collection of elements which we denote by $\mathcal{T}^0_\Gamma$ whose union forms the domain $\Gamma_{h,0}$ which satisfies the assumptions ???.
We evolve the Lagrange nodes $X_i$ by solving the ordinary differential equation
\[
  \dot{X}_i = w(X_i, t).
\]
We further assume that the nodes ``fitted'' to $\partial \Omega$ do not move.
We define the discrete velocity $W^j \in V_h^d$ by
\begin{equation}
  W^j = \frac{1}{\tau} \sum_{l=0}^q \delta_l X^{j-l},
\end{equation}
where ${ \delta_l }_{l=0}^q$ are the backward difference formula weights determined from the relation:
\begin{equation}
  \label{eq:bdf-weights}
  \delta(\zeta) = \sum_{l=0}^q \delta_j \zeta^q
  = \sum_{\lambda=1}^q \frac{1}{\lambda} (1 - \zeta)^\lambda.
\end{equation}

We define our broken finite element spaces $S^h(t_i)$ as \cref{eq:??} over the triangulation $\mathcal{J}^h(t_i)$ for each time step $t_i, i=0, \ldots, M$.
Given starting values $U^0 \in S^h(t_0), \ldots, U^{q-1} \in S^h(t_0)$, and appropriate data $f_h, g_h, A$, for $j = q, \ldots, M$, we wish to find $U^j \in S^h(t_j)$ as the solution of
%
\begin{multline}
  \label{eq:full-discrete}
  \sum_{l=0}^q \int_\Omega \delta_{l} U^{j-l} \chi_i^{j-l}
  + \int_\Omega U^j W^j \cdot \nabla \chi_i^{j} + D \nabla U^j \cdot \nabla \chi_i^j \\
  =  \int_{\Omega} f_h \chi_i^j + \int_{\Gamma_h} g_h \chi_i^j|_{\Gamma_h}
  \qquad \mbox{ for all, basis function } \chi_i^j \in V_h^j,
\end{multline}
where again $\delta_l$ are the BDF weights \cref{eq:bdf-weights}.

\subsection{Numerical examples of advection-diffusion problem}

For $d = 2, 3$, let $\Omega = [-1,1]^d$, for $t \in [0,T]$, we define the evolution of the domain through the flow map $\Phi_t$ given by:
%
\begin{align*}
  \Phi_t(x) = x + \frac{|x|^{1/3}\prod_{i=1}^d(1-x_i^2)}{0.5 \prod_{i=1}^d (1-4 x_i^2 / |x|)}
  \begin{cases}
    ((\alpha(t)-1) x_1, (\beta(t)-1) x_2) & \mbox{ if } d = 2, \\
    ((\alpha(t)1-) x_1, (\beta(t)-1) x_2, 0) & \mbox{ if } d = 3.
  \end{cases}
\end{align*}
This is a special motion which ensures that notes initially on $\partial \Omega$ do not move and the surface $\Gamma(t)$ is described by the level set function $\phi(\cdot, t)$ given by
\begin{align*}
  \phi(\cdot, t) = \begin{cases}
    \frac{x_1^2}{\alpha(t)^2} + \frac{x_2^2}{\beta(t)^2} - \frac{1}{2}
    & \mbox{ if } d = 2 \\
    \frac{x_1^2}{\alpha(t)^2} + \frac{x_2^2}{\beta(t)^2} + x_3^2 - \frac{1}{2}
    & \mbox{ if } d = 3.
   \end{cases}
\end{align*}

We define $\Omega_1(t)$ as the interior of $\Gamma(t)$ and $\Omega_2(t) = \Omega \setminus \Omega_1(t)$.

We set $A|_{\Omega_1(t)} = 10$ and $A_{\Omega_2(t)} = 1$.
We construct $f$ and $g$ such that the exact solution $u$ is given by
\[
  u(x, t) =
  \sin(t) |\Phi(x)| \prod_{i=1}^d (x_i^2 - 1) \cos(4 x_i).
\]
We compute using isoparametric elements of order 1, 2, 3 on a fixed mesh to find a solution $u_h$.
For order $k$ discretisation in space we use BDF order $k+1$ in time.
The initial solution $U^0 = 0$ matches the exact solution at $t = 0$. The other starting values are computed using a lower order BDF method.
For elements of order $k$ we expect convergence of order $k+1$ for the error $u - u_h$ in the $L^2(\Omega)$ norm.
The results are shown in \cref{fig:results-advection-diffusion} for the cases $d=2,3$ respectively.
The precise numerical values are shown in \cref{tab:advection-diffusion-2d,tab:advection-diffusion-3d}.

\begin{figure}[tbh]
  \includegraphics{output/advection-diffusion-2d-h-l2_error}
  \includegraphics{output/advection-diffusion-3d-h-l2_error}
  \caption{$L^2$ error for advection-diffusion problem for $d=2$ (left) and $d=3$ (right).}
  \label{fig:results-advection-diffusion}
\end{figure}

\begin{figure}[tbh]
  \includegraphics{output/advection-diffusion-2d-h-h1_error}
  \includegraphics{output/advection-diffusion-3d-h-h1_error}
  \caption{$H^1$ error for advection-diffusion problem for $d=2$ (left) and $d=3$ (right).}
  \label{fig:results-advection-diffusion2}
\end{figure}

\begin{figure}[tbh]
  \includegraphics{output/advection-diffusion-2d-h-l2_error:h1_error.pdf}
  \includegraphics{output/advection-diffusion-3d-h-l2_error:h1_error.pdf}
  \caption{$L^2$ error (solid) and $H^1$ error(dashed) for advection-diffusion problem for $d=2$ (left) and $d=3$ (right).}
  \label{fig:results-advection-diffusion2}
\end{figure}




\begin{table}[p]
  \begin{subtable}{\textwidth}
  \centering
  \include{output/advection-diffusion-order1-2d.tex}
  \caption{Order 1, $d=2$}
\end{subtable}

\begin{subtable}{\textwidth}
  \centering
  \include{output/advection-diffusion-order2-2d.tex}
  \caption{Order 2, $d=2$}
\end{subtable}

\begin{subtable}{\textwidth}
  \centering
  \include{output/advection-diffusion-order3-2d.tex}
  \caption{Order 2, $d=2$}
\end{subtable}

\caption{Numerical results for the advection-diffusion problem for $d=2$}
\label{tab:advection-diffusion-2d}
\end{table}

\begin{table}[p]
\begin{subtable}{\textwidth}
  \centering
  \include{output/advection-diffusion-order1-3d.tex}
  \caption{Order 1, $d=3$}
\end{subtable}

\begin{subtable}{\textwidth}
  \centering
  \include{output/advection-diffusion-order2-3d.tex}
  \caption{Order 2, $d=3$}
\end{subtable}

\begin{subtable}{\textwidth}
  \centering
  \include{output/advection-diffusion-order3-3d.tex}
  \caption{Order 3, $d=3$}
\end{subtable}

\caption{Results for advection-diffusion problem for $d=3$.}
\label{tab:advection-diffusion-3d}
\end{table}

\bibliographystyle{plain}
\bibliography{report/library.bib}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
