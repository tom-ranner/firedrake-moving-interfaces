# Firedrake moving interface

[![DOI](https://zenodo.org/badge/DOI/10.5281/zenodo.6827046.svg)](https://doi.org/10.5281/zenodo.6827047)

This is a collection of python scripts that together can be used to run an advection-diffusion problem in a domain with a moving interface separating the domain into two regions using the finite element package [Firedrake](https://www.firedrakeproject.org).

## Singularity container

A [singularity container](https://sylabs.io/singularity) to run the code can be built using the file `singularity.def`:

    $ sudo singularity build firedrake.sif singularity.def

This script installs `firedrake` in a container for easy of use. Once the script has been build the script `firedrake-python` can used as a drop in for the usual `python`(`python3`) executable in an environment with `firedrake` installed. e.g.:

    $ ./firedrake-python advection-diffusion.py --mesh=my_mesh.msh

Note: it should be possible to run in parallel as well but this is untested.

## Paper results

[![Snakemake](https://img.shields.io/badge/snakemake-6.4-brightgreen.svg?style=flat)](https://snakemake.readthedocs.io)

In order to generate all results used for the paper the precise instructions are set up in `workflow/Snakefile` using the [Snakemake](https://snakemake.readthedocs.io/en/stable/index.html) workflow management system. This requires further dependencies listed in `environment.yml` which can be installed using `conda`:

    $ conda env install -f environment.yml
    $ conda activate firedrake-moving-interfaces

Once the dependencies are installed, paper results can be generated using (here, for example, using 8 cores):

    $ snakemake -j8 all

It is also possible to run snakemake through a web browser gui by running:

    snakemake -j8 --gui

The `Snakefile` also gives examples of how to run the code.

# Contributing

Pull requests with fixes or additional features are welcome. Please ensure formatting passes the ci tests.
